import { Model } from 'models'

const categoriaEvento = Model({
  tableName: 'categorias_eventos',
  uuid: true
})

export default categoriaEvento
