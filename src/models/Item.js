import bookshelf, { Model } from 'models'
import categoriaProduto from 'models/CategoriaProduto'

const item = Model({
  tableName: 'items',
  uuid: true,
  toJSON: function () {
    const { ...item } = bookshelf.Model.prototype.toJSON.apply(this, arguments)
    return item
  },
  categoria_produto_id: function () {
    return this.belongsTo(categoriaProduto, 'categoria_produto_id')
  }
})

export default item
