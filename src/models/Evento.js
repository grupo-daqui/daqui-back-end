import bookshelf, { Model } from 'models'
import categoriaEvento from 'models/CategoriaEvento'

const evento = Model({
  tableName: 'eventos',
  uuid: true,
  toJSON: function () {
    const { ...evento } = bookshelf.Model.prototype.toJSON.apply(
      this,
      arguments
    )
    return evento
  },
  categoria_evento_id: function () {
    return this.belongsTo(categoriaEvento, 'categoria_evento_id')
  }
})

export default evento
