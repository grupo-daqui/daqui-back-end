import Joi from '@hapi/joi'

import { validateSchema } from 'helpers'

const PessoasFisicas = {
  create: () =>
    validateSchema({
      body: {
        nome: Joi.string().required(),
        apelido: Joi.string().required(),
        email: Joi.string().email().required()
      }
    }),

  update: () =>
    validateSchema({
      body: {
        nome: Joi.string().required(),
        apelido: Joi.string().required(),
        email: Joi.string().email().required()
      }
    })
}

export default PessoasFisicas
