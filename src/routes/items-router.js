import Router from 'koa-router'
import ItemController from '../controllers/item-controller'

const router = new Router()

router.get('/items', ItemController.index)
router.get('/items/:uuid', ItemController.show)

export default router.routes()
