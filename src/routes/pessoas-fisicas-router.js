import Router from 'koa-router'

import PessoaFisicaController from 'controllers/pessoa-fisica-controler'
import PessoaFisicaValidate from 'validators/pessoas-fisicas-schema'

const router = new Router()

router.get('/pessoas-fisicas', PessoaFisicaController.index)
router.get('/pessoas-fisicas/:uuid', PessoaFisicaController.show)
router.post(
  '/pessoas-fisicas/',
  PessoaFisicaValidate.create(),
  PessoaFisicaController.create
)
router.put(
  '/pessoas-fisicas/:id',
  PessoaFisicaValidate.update(),
  PessoaFisicaController.update
)
router.delete('/pessoas-fisicas/:id', PessoaFisicaController.destroy)

export default router.routes()
