import Router from 'koa-router'
import EventoController from '../controllers/evento-controller'

const router = new Router()

router.get('/eventos', EventoController.index)
router.get('/eventos/:uuid', EventoController.show)

export default router.routes()
