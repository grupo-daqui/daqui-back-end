import Router from 'koa-router'
import CategoriaEventoController from 'controllers/categoria-evento-controller'

const router = new Router()

router.get('/categorias-eventos', CategoriaEventoController.index)
router.get('/categorias-eventos/:id', CategoriaEventoController.show)

export default router.routes()
