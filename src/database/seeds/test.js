import uuidv4 from 'uuid/v4'

export const seed = async knex => {
  await knex('pessoas_fisicas').del()
  await knex('pessoas_fisicas').insert([
    {
      uuid: '952f35d9-15d9-49c5-974a-68a1946070bd',
      nome: 'Michel Silva',
      apelido: 'mickey',
      email: 'michelsilva@gmail.com'
    },
    {
      uuid: '152f35d9-15d9-49c5-974a-68a1946070bd',
      nome: 'João Moura',
      apelido: 'Mourão',
      email: 'Mourão@gmail.com'
    }
  ])
  await knex('estabelecimentos').del()
  await knex('estabelecimentos').insert([
    {
      uuid: '152f35d9-15d9-49c5-974a-68a1946070bc',
      nome: 'alemão',
      endereco: 'assis brasil 6',
      telefone: '99953555',
      horario: '20:30 03:00',
      cidade: 'Pedro Osorio',
      descricao: 'venha comer na melhor lancheria da cidade',
      pessoa_fisica_uuid: '952f35d9-15d9-49c5-974a-68a1946070bd',
      categoria_estabelecimento_id: 1,
      coordenadas: '(0.0, 0.0)'
    }
  ])
  await knex('items').del()
  await knex('items').insert([
    {
      estabelecimento_uuid: '152f35d9-15d9-49c5-974a-68a1946070bc',
      categoria_produto_id: 1,
      nome: 'xis salada',
      preco: 10,
      descricao: 'milho, ervilha, bife, maionese, etc..'
    }
  ])
  await knex('servicos').del()
  await knex('servicos').insert([
    {
      uuid: '352f35d9-15d9-49c5-974a-68a1946070bd',
      telefone: '32551121',
      nome: 'Mourao eletricista',
      horario_atendimento: '10:00 as 20:00',
      descricao: 'melhor eletricista da cidade pode confiar é eletrimourao',
      categoria_servico_id: 5,
      pessoa_fisica_uuid: '152f35d9-15d9-49c5-974a-68a1946070bd'
    }
  ])
  await knex('eventos').del()
  await knex('eventos').insert([
    {
      uuid: '552f35d9-15d9-49c5-974a-68a1946070bd',
      nome: 'Festa da melancia 2020',
      data: '2020-08-02',
      descricao:
        'Vem aí a XIX Expofesta Regional da Melancia e 10ª Feira Regional da Agricultura Familiar de Pedro Osório. O maior evento da região sul do estado ocorre nos dias 8 e 9 de fevereiro, no parque do Sindicato Rural.',
      horario: '10:00 as 20:00',
      local: 'Agricola',
      coordenadas: '(-31.875584, -52.819321)',
      categoria_evento_id: 5
    }
  ])
}
