export const seed = async (knex, Promise) => {
  await knex('categorias_estabelecimentos').del()
  await knex('categorias_estabelecimentos').insert([
    { id: 1, nome: 'lancheria' },
    { id: 2, nome: 'farmacia' },
    { id: 3, nome: 'agropecuaria' },
    { id: 4, nome: 'farmacia' }
  ])
  await knex('categorias_produtos').del()
  await knex('categorias_produtos').insert([
    { id: 1, nome: 'lanche' },
    { id: 2, nome: 'pizza' },
    { id: 3, nome: 'roupa' },
    { id: 4, nome: 'farmaco' },
    { id: 5, nome: 'bebida' }
  ])
  await knex('categorias_servicos').del()
  await knex('categorias_servicos').insert([
    { id: 1, nome: 'Arquiteto' },
    { id: 2, nome: 'DJ' },
    { id: 3, nome: 'Designer' },
    { id: 4, nome: 'Desenvolvedor' },
    { id: 5, nome: 'Eletricista' }
  ])
  await knex('categorias_eventos').del()
  await knex('categorias_eventos').insert([
    { id: 1, nome: 'Social' },
    { id: 2, nome: 'Corporativo' },
    { id: 3, nome: 'Comunitario' },
    { id: 4, nome: 'Academico' },
    { id: 5, nome: 'Cultural' },
    { id: 6, nome: 'Esportivo' },
    { id: 7, nome: 'Politico' },
    { id: 8, nome: 'Educacional' }
  ])
}
