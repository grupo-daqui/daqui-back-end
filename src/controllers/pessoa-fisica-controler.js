import PessoaFisica from 'models/PessoaFisica'
import { NotFound } from '../helpers'

export const index = () => new PessoaFisica().fetchAll()

export const show = ctx => new PessoaFisica({ uuid: ctx.params.uuid }).fetch()

export const update = async ctx => {
  const { body } = ctx.request

  return new PessoaFisica({ id: ctx.params.id }).save(
    {
      nome: body.nome,
      email: body.email,
      apelido: body.apelido
    },
    { method: 'update' }
  )
}

export const create = async ctx => {
  const { body } = ctx.request

  return new PessoaFisica({
    nome: body.nome,
    email: body.email,
    apelido: body.apelido
  }).save()
}

export const destroy = ctx => new PessoaFisica({ id: ctx.params.id }).destroy()

export default {
  index,
  show,
  create,
  update,
  destroy
}
