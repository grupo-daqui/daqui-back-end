import Evento from 'models/Evento'
import { NotFound } from '../helpers'

export const index = () => new Evento().fetchAll()

export const show = ctx => new Evento({ uuid: ctx.params.uuid }).fetch()

export default {
  index,
  show
}
