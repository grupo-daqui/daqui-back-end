import CategoriaEvento from 'models/CategoriaEvento'
import { NotFound } from '../helpers'

export const index = () => new CategoriaEvento().fetchAll()

export const show = ctx => new CategoriaEvento({ id: ctx.params.id }).fetch()

export default {
  index,
  show
}
