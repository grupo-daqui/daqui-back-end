import Item from 'models/Item'
import { NotFound } from '../helpers'

export const index = () => new Item().fetchAll()

export const show = ctx => new Item({ uuid: ctx.params.uuid }).fetch()

export default {
  index,
  show
}
